package com.sasanebrahimi.loginsampleapp.business.validator

interface EmailValidator {

    fun check(email: String): EmailValidationResult

    enum class EmailValidationResult {
        VALID,
        EMPTY,
        INVALID
    }

}