package com.sasanebrahimi.loginsampleapp.business.validator

interface PasswordValidator {

    fun check(password: String): PasswordValidationResult

    enum class PasswordValidationResult {
        VALID,
        SHORT,
        WEAK
    }

}