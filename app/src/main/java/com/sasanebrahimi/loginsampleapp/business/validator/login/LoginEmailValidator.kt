package com.sasanebrahimi.loginsampleapp.business.validator.login

import com.sasanebrahimi.loginsampleapp.business.validator.EmailValidator

class LoginEmailValidator :
    EmailValidator {

    companion object{
        private const val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    }

    override fun check(email: String): EmailValidator.EmailValidationResult {
        if(email.matches(Regex(emailPattern))) return EmailValidator.EmailValidationResult.VALID
        else if (email.trim().length == 0) return EmailValidator.EmailValidationResult.EMPTY
        else return EmailValidator.EmailValidationResult.INVALID
    }

}