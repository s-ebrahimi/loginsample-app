package com.sasanebrahimi.loginsampleapp.business.validator.login

import com.sasanebrahimi.loginsampleapp.business.validator.PasswordValidator
import java.util.regex.Pattern

class LoginPasswordValidator :
    PasswordValidator {

    companion object{
        private const val passwordPattern =
      "^" +
        "(?=.*[0-9])" +         //at least 1 digit
        "(?=.*[a-z])" +         //at least 1 lower case letter
        "(?=.*[A-Z])" +         //at least 1 upper case letter
        "(?=.*[a-zA-Z])" +      //any letter
        "(?=.*[@#$%^&+=])" +    //at least 1 special character
        "(?=\\S+$)" +           //no white spaces
        ".{4,}" +               //at least 4 characters
        "$";
    }

    override fun check(password: String): PasswordValidator.PasswordValidationResult {
        if (password.trim().length > 11){

            if(password.matches(Regex(passwordPattern))) return PasswordValidator.PasswordValidationResult.VALID
            return PasswordValidator.PasswordValidationResult.WEAK
        }else{
            return PasswordValidator.PasswordValidationResult.SHORT
        }
    }

}