package com.sasanebrahimi.loginsampleapp.base.ui

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.sasanebrahimi.loginsampleapp.R

open class BaseActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        supportActionBar?.hide()
        super.onCreate(savedInstanceState)
    }

}