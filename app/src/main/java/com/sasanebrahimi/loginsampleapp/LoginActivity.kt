package com.sasanebrahimi.loginsampleapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import com.jakewharton.rxbinding3.widget.textChangeEvents
import com.sasanebrahimi.loginsampleapp.base.ui.BaseActivity
import com.sasanebrahimi.loginsampleapp.business.validator.EmailValidator
import com.sasanebrahimi.loginsampleapp.business.validator.PasswordValidator
import com.sasanebrahimi.loginsampleapp.business.validator.login.LoginEmailValidator
import com.sasanebrahimi.loginsampleapp.business.validator.login.LoginPasswordValidator
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    private val emailValidator = LoginEmailValidator()
    private val passwordValidator = LoginPasswordValidator()

    companion object {
        private const val TAG = "LoginActivity"

        /**
         * static method to start this activity
         * all other activities and components should use this method to start this activity
         * helpful to have control on extras coming to this activity
         */
        fun start(context: Context, exampleParam: String = "") {
            val starter = Intent(context, LoginActivity::class.java)
            starter.putExtra("exampleParam", exampleParam)
            context.startActivity(starter)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setClickListeners()
        setObservers()
    }


    private fun setClickListeners() {
        btn_dark_theme.setOnClickListener {
            if (AppCompatDelegate.getDefaultNightMode() != AppCompatDelegate.MODE_NIGHT_YES) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                finish()
            }
        }
        btn_light_theme.setOnClickListener {
            if (AppCompatDelegate.getDefaultNightMode() != AppCompatDelegate.MODE_NIGHT_NO) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                finish()
            }
        }
    }

    private fun setObservers() {

        var emailObservable = ti_email.textChangeEvents()
            .skip(1)
            .map {
                return@map it.text
            }.map {
                it.toString()
            }.map {
                emailValidator.check(it.trim())
            }
            .observeOn(AndroidSchedulers.mainThread())

        emailObservable.subscribe {
            when (it) {
                EmailValidator.EmailValidationResult.VALID -> til_email.error = null
                EmailValidator.EmailValidationResult.INVALID -> til_email.error = "Invalid Email"
            }
        }

        var passwordObservable = ti_password.textChangeEvents()
            .skip(1)
            .map {
                return@map it.text
            }
            .map {
                it.toString()
            }.map {
                passwordValidator.check(it.trim())
            }


        passwordObservable.subscribe {
            when (it) {
                PasswordValidator.PasswordValidationResult.VALID -> til_password.error = null
                PasswordValidator.PasswordValidationResult.SHORT -> til_password.error =
                    "The email should be at least 12 characters"
                PasswordValidator.PasswordValidationResult.WEAK -> til_password.error =
                    "The email should be a combination of Capital letter, Number and  Symbol"
            }
        }


        /**
         * To get notified whenever both inputs are valid
         */
        Observable.combineLatest(
            emailObservable,
            passwordObservable,
            BiFunction<EmailValidator.EmailValidationResult, PasswordValidator.PasswordValidationResult, Boolean> { u, p ->
                if (u == EmailValidator.EmailValidationResult.VALID && p == PasswordValidator.PasswordValidationResult.VALID)
                    return@BiFunction true
                return@BiFunction false
            })
            .subscribe {
                Log.d(TAG, "setObservers: $it");
                if(it == true) Toast.makeText(this@LoginActivity, "Inputs are valid !!!", Toast.LENGTH_SHORT).show()

            }



    }


}