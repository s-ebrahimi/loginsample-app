# Login Sample App 

This is a sample android project to show form validation check and also using dark and ligh themes.

## Features

 -  Project is implemented in  kotlin
 -  Dark and Light Themes could be chosen in app
 -  Email and Password inputs are checked while user is typing, shows appropriate notice message if input is not valid
 -  whenever both Email and Passwords are valid, a Toast message is shown
 
 # Used Libraries
- **RxJava**, **RxAndroid**: To Observe email and password inputs to show appropriate error messages (input is invalid). Also to combine these observable to observe all inputs and show a Toast message whenever all inputs are valid

- **Coroutine**: To make delay on Splach screen
- **Android Lifecycle**: To run Coroutines in activity lifecycle
- **Material**: To use Material components in ui



## Screnshots

||||
|--|--|--|
| ![enter image description here](https://gitlab.com/s-ebrahimi/loginsample-app/-/raw/master/screenshots/light_error.png) | ![enter image description here](https://gitlab.com/s-ebrahimi/loginsample-app/-/raw/master/screenshots/light_valid.png) |![enter image description here](https://gitlab.com/s-ebrahimi/loginsample-app/-/raw/master/screenshots/dark.png)|
